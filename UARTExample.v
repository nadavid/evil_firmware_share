//-----------------------------------------------------------------------------
// Title         : UARTExample
// Project       : UARTExample
//-----------------------------------------------------------------------------
// File          : UARTExample.v
// Author        : Vlad Negnevitsky  <vlad@vlad-VirtualBox>
// Created       : 07.11.2012
// Last modified : 07.11.2012
//-----------------------------------------------------------------------------
// Description :
// 
//-----------------------------------------------------------------------------
// Copyright (c) 2012 by TIQI, ETHZ This model is the confidential and
// proprietary property of TIQI, ETHZ and the possession or use of this
// file requires a written license from TIQI, ETHZ.
//------------------------------------------------------------------------------
// Modification history :
// 07.11.2012 : created
//-----------------------------------------------------------------------------

module UARTExample(
		   input extclk,
		   input  rx,
		   output tx
		   );

   reg 			  en_16_x_baud;
   reg [3:0] 		  baud_count;

   wire [7:0] 		  dout;
   wire 		  data_present, clk;
   
   initial begin
      baud_count = 0;
   end
   
   always @(posedge clk) begin
      if (baud_count == 4'd1) begin
	 baud_count <= 4'd0;
	 en_16_x_baud <= 1'd1;
      end else begin
	 baud_count <= baud_count + 4'd1;
	 en_16_x_baud <= 1'd0;
      end
   end

   dcm_32to96 Inst_dcm32to96(
      .CLKIN_IN(extclk),
      .CLKFX_OUT(clk),
      .CLKIN_IBUFG_OUT(),
      .CLK0_OUT()
	);

   uart_tx Inst_uart_tx(
			.data_in(dout),
			.write_buffer(data_present),
			.reset_buffer(1'd0),
			.en_16_x_baud(en_16_x_baud),
			.clk(clk),
			.serial_out(tx),
			.buffer_half_full(),
			.buffer_full()
			);

   uart_rx Inst_uart_rx(
			.serial_in(rx),
			.read_buffer(1'd1),
			.reset_buffer(1'd0),
			.en_16_x_baud(en_16_x_baud),
			.clk(clk),
			.data_out(dout),
			.buffer_data_present(data_present),
			.buffer_half_full(),
			.buffer_full()
			);

endmodule // UARTExample
