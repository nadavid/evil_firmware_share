//-----------------------------------------------------------------------------
// Title         : lp_filter_tb
// Project       : EVIL
//-----------------------------------------------------------------------------
// File          : lp_filter_tb.v
// Author        : Alexander Hungenberg  <ahungenberg@phys.ethz.ch>
// Created       : 08.04.2013
// Last modified : 08.04.2013
//-----------------------------------------------------------------------------
// Description :
// Testbench for lp_filter
//-----------------------------------------------------------------------------
// Copyright (c) 2013 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 08.04.2013 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ns

`ifndef _LP_FILTER_TB_
 `define _LP_FILTER_TB_

 `ifndef _LP_FILTER_
  `include "lp_filter.v"
 `endif

module lp_filter_tb;

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg			clk;			// To UUT of lp_filter.v
   reg signed [15:0]	data_i;			// To UUT of lp_filter.v
   reg [15:0]		smoothing_i;		// To UUT of lp_filter.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire signed [15:0]	data_o;			// From UUT of lp_filter.v
   // End of automatics

   
/* -----\/----- EXCLUDED -----\/-----
   wire signed [15:0] 	data_o_r;
   wire signed [15:0] 	data_o = data_o_r;   
 -----/\----- EXCLUDED -----/\----- */

   lp_filter UUT(/*AUTOINST*/
		 // Outputs
		 .data_o		(data_o[15:0]),
		 // Inputs
		 .clk			(clk),
		 .data_i		(data_i[15:0]),
		 .smoothing_i		(smoothing_i[15:0]));

   always #5 clk = !clk;
   always #200000 data_i = -data_i;
   
   initial begin
      $dumpfile("icarus_compile/000_lp_filter_tb.vcd");
      $dumpvars(0,lp_filter_tb);

      clk = 1;
      data_i = 100;
      smoothing_i = 1000;

      #10000000 $finish();
   end // initial begin

/* -----\/----- EXCLUDED -----\/-----
   reg [15:0] data_o_r2;
   always @(posedge clk) begyin
      data_o_r2 <= data_o;
   end
   wire [15:0] data_o_deriv = data_o-data_o_r2;
 -----/\----- EXCLUDED -----/\----- */

endmodule // lp_filter_tb

`endif
