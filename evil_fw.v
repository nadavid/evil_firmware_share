//-----------------------------------------------------------------------------
// Title         : evil_fw
// Project       : evil_fw
//-----------------------------------------------------------------------------
// File          : evil_fw.v
// Author        : Vlad Negnevitsky  <vlad@vlad-VirtualBox>
// Created       : 12.11.2012
// Last modified : 12.11.2012
//-----------------------------------------------------------------------------
// Description :
// Top-level wrapper of Evil_fw, or LuPID Lite.
//-----------------------------------------------------------------------------
// Copyright (c) 2012 by TIQI, ETHZ This model is the confidential and
// proprietary property of TIQI, ETHZ and the possession or use of this
// file requires a written license from TIQI, ETHZ.
//------------------------------------------------------------------------------
// Modification history :
// 12.11.2012 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ps

`ifndef _EVIL_FW_
 `define _EVIL_FW_

module evil_fw(
               input              clk_i,
               input              trig_in,

               // ADCs
               input signed [9:0] adc_a_i,
               input signed [9:0] adc_b_i,
               output             adc_clk_o,
               output             adc_pdwn_o,

               input              adc_a_overflow,
               input              adc_b_overflow,

               // Parallel DAC
               output [13:0]      fast_dac_o,
               output             fast_dac_clk_o,

               // Slow DAC
               output             slow_dac_serial_data_o,
               output             slow_dac_clk_o,
               output             slow_dac_sync_o,

               // RS232
               input              rxd_i,
               output             txd_o,

               // Debugging
               // | 0 | 3 |
               // | 1 | 2 |
               // EXTERNAL 
               //   I/O
               inout [3:0]        dbg
               );
   
   // Firmware version (last two digits are decimal)
   // Major releases (changes to register mapping etc) should be in the hundreds.
   parameter FIRMWARE_VERSION = 140;

   wire                           clk;  
   wire                           clk_w;
   reg                            en_16_x_baud = 0;
   reg [3:0]                      baud_count = 0;

   wire [7:0]                     rx_data, tx_data;
   wire                           rx_ready, tx_ready;
   
   // m := multiplexed
   reg [15:0]                     m_ramp_centre = 0;
   reg                            m_pid_rst_n = 0;
   // Control wires
   wire [15:0]                    system_control, ramp_range, ramp_speed, p_gain, i_gain, d_gain;
   wire signed [15:0]             peak_smoothing;
   wire [15:0]                    peak_threshold, peak_detection_ttl;
   wire signed [15:0]             ramp_centre, in_offset, out_offset;
   wire                           pid_rst_n = system_control[0]; // 1: PID is active; 0: PID is off
   wire                           ramp_rst_n = system_control[1]; // 1: ramp is active; 0: ramp is off
   wire                           output_sel = system_control[2]; // 1: output is ramp; 0: output is PID
   wire                           pid_polarity = system_control[3]; // PID polarity
   assign adc_pdwn_o = system_control[4]; // When true, powers down the ADCs
   wire                           peak_detect_on = system_control[5];

   // DSP wires
   wire signed [15:0]             ramp, pid;

   // Ramp trigger output
   wire                           ramp_trig;

   // Peak Detector Output
   wire                           peak_detected;
   wire [15:0]                    peak_slow_filter, peak_fast_filter;
   
   ///////////////////// UART comms /////////////
   
   always @(posedge clk) begin
      if (baud_count == 4'd1) begin
         baud_count <= 4'd0;
         en_16_x_baud <= 1'd1;
      end else begin
         baud_count <= baud_count + 4'd1;
         en_16_x_baud <= 1'd0;
      end
   end

   uart_rx Inst_uart_rx(
                        .serial_in(rxd_i),
                        .read_buffer(1'd1),
                        .reset_buffer(1'd0),
                        .en_16_x_baud(en_16_x_baud),
                        .clk(clk),
                        .data_out(rx_data),
                        .buffer_data_present(rx_ready),
                        .buffer_half_full(),
                        .buffer_full()
                        );
   
   uart_tx Inst_uart_tx(
                        .data_in(tx_data),
                        .write_buffer(tx_ready),
                        .reset_buffer(1'd0),
                        .en_16_x_baud(en_16_x_baud),
                        .clk(clk),
                        .serial_out(txd_o),
                        .buffer_half_full(),
                        .buffer_full()
                        );

   ////////////////////// end UART comms /////////////
   
   dcm_32to96 dcm_32to96_inst(
                              .CLKIN_IN(clk_i), 
                              .CLKFX_OUT(clk), 
                              .CLKIN_IBUFG_OUT(), 
                              .CLK0_OUT(clk_w)
                              );
   
   assign adc_clk_o = ~clk;
   
   assign fast_dac_clk_o = ~clk; // negedge is when data is stable
   
   (*IOB="TRUE"*) reg [9:0]             adc_a_r, adc_b_r; // registered ADC inputs
   reg [8:0] adc_a_r_rnd, adc_b_r_rnd; // rounded ADC inputs
   (*IOB="TRUE"*) reg [13:0] fast_dac_r;
   assign fast_dac_o = fast_dac_r;
   
   initial begin
      adc_a_r = 10'd0;
      adc_b_r = 10'd0;
      fast_dac_r = {1'd1,13'd0};
   end
   
   always @(posedge clk) begin
      adc_a_r <= adc_a_i;
      adc_b_r <= adc_b_i;
      adc_a_r_rnd <= adc_a_r[9:1]+9'd1; // round upwards
      adc_b_r_rnd <= adc_b_r[9:1]+9'd1; // round upwards
      if (output_sel) fast_dac_r <= {~ramp[15],ramp[14:2]};
      else fast_dac_r <= {~pid[15],pid[14:2]};
   end
   
   //assign dbg[0] = adc_a_overflow;
   //assign dbg[1] = adc_a_r[9];
   //assign dbg[2] = adc_a_r[8];
   //assign dbg[3] = fast_dac_o[13];
   assign dbg=0;

   evil_decoder #(
                  .VERSION(FIRMWARE_VERSION)
                  )evil_decoder_i(// Outputs
                                  .tx_data_o   (tx_data),
                                  .tx_ready_o  (tx_ready),
                                  .str0_o      (),
                                  .str1_o      (),
                                  .str2_o      (),
                                  .str3_o      (),
                                  .str4_o      (),
                                  .str5_o      (),
                                  .str6_o      (),
                                  .str7_o      (),
                                  .reg0_o      (system_control),
                                  .reg1_o      (ramp_centre),
                                  .reg2_o      (ramp_range),
                                  .reg3_o      (ramp_speed),
                                  .reg4_o      (in_offset),
                                  .reg5_o      (out_offset),
                                  .reg6_o      (p_gain),
                                  .reg7_o      (i_gain),
                                  .reg8_o      (d_gain),
                                  .reg9_o      (peak_smoothing),
                                  .reg10_o     (peak_threshold),
                                  .reg11_o     (peak_detection_ttl),
                                  .reg12_o     (),
                                  .reg13_o     (),
                                  .reg14_o     (),
                                  .reg15_o     (),
                                  // Inputs
                                  .clk      (clk),
                                  .rx_data_i   (rx_data),
                                  .rx_ready_i  (rx_ready),
                                  .ramp_trig_i(ramp_trig),
                                  .str0_i      (adc_a_r_rnd[8:1]),
                                  .str1_i      (adc_b_r_rnd[8:1]),
                                  .str2_i      (ramp[15:8]),
                                  .str3_i      (pid[15:8]),
                                  .str4_i      (pid[7:0]),
                                  .str5_i      (peak_slow_filter[15:8]),
                                  .str6_i      (peak_fast_filter[7:0]),
                                  .str7_i      (8'd0));

   //////////////////// RAMP GENERATOR ////////////////
     ramp_gen ramp_gen_i(
                         .clk_i(clk),
                         .rst_n_i(ramp_rst_n),
                         .centre_i(ramp_centre),
                         .range_i(ramp_range),
                         .speed_i(ramp_speed),
                         .ramp_trig_o(ramp_trig),
                         .ramp_o(ramp)
                         );

   

   //////////////////// PID /////////////////////
     digi_pid digi_pid_i(
                         .clk(clk),
                         .err_i(adc_a_r),
                         .p_i(p_gain),
                         .i_i(i_gain),
                         .d_i(d_gain),
                         .in_offset_i(in_offset),
                         .out_offset_i(out_offset),
                         .sw_polar_i(pid_polarity),
                         .rst_n_i(m_pid_rst_n),
                         .ramp_centre_i(m_ramp_centre),
                         .pid_o(pid)
                         );
   
   //////////////////// SLOW DAC-Connect to ADC a /////////////////////
     spi_dac spi_dac_i (
                        .I_CLK_REF(clk), 
                        //.SPI_DATA({adc_a_r,2'd0}),
                        .SPI_DATA({~fast_dac_r[13],fast_dac_r[12:2]}),
                        .DAC_SDATA(slow_dac_serial_data_o), 
                        .DAC_SYNC(slow_dac_sync_o), 
                        .DAC_CLK(slow_dac_clk_o)
                        );

   //////////////////// PEAK DETECTION MODULE //////////////////////
       lp_peak_detector lp_peak_detector_i(
                                           .clk(clk),
                                           .data_i(pid),
                                           .smoothing_i(peak_smoothing),
                                           .peak_threshold_i(peak_threshold),
                                           .peak_detected_ttl_i(peak_detection_ttl),
                                           .peak_detected_rst_i(peak_detect_on),
                                           .peak_detected_o(peak_detected),
                                           .data_slow_lp_o(peak_slow_filter),
                                           .data_i_filtered_o(peak_fast_filter)
                                           );
   
   
   always @(posedge clk) begin
      if (peak_detected) begin
         m_ramp_centre <= peak_slow_filter;
         m_pid_rst_n <= 0;
      end else begin
         m_ramp_centre <= ramp_centre;
         m_pid_rst_n <= pid_rst_n;
      end
   end
   
endmodule // evil_fw

`endif //  `ifndef _EVIL_FW_
