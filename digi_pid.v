//-----------------------------------------------------------------------------
// Title         : digi_pid
// Project       : EVIL
//-----------------------------------------------------------------------------
// File          : digi_pid.v
// Author        : Ludwig de Clercq & Vlad. Negnevitsky
// Created       : 02.11.2012
// Last modified : 02.11.2012
//-----------------------------------------------------------------------------
// Description :
// Direct clone of DIGISIMPLEv2.vhd
//-----------------------------------------------------------------------------
// Copyright (c) 2012 by TIQI, ETHZ This model is the confidential and
// proprietary property of TIQI, ETHZ and the possession or use of this
// file requires a written license from TIQI, ETHZ.
//------------------------------------------------------------------------------
// Modification history :
// 02.11.2012 : created
//-----------------------------------------------------------------------------

`ifndef _DIGI_PID_
 `define _DIGI_PID_

module digi_pid(
		input 		    clk,
		input signed [9:0]  err_i,
		input [15:0] 	    p_i,
		input [15:0] 	    i_i,
		input [15:0] 	    d_i,
		input signed [15:0] in_offset_i,
		input signed [15:0] out_offset_i,
		input 	    sw_polar_i,
		input	    rst_n_i,

		input [15:0] 	    ramp_centre_i,
		output [15:0] 	    pid_o
		);

   // Total bits of accumulator (i.e. attenuation of both proportional and integrator)
   // Working for ACC_BITS = 46, P_SHIFT = 20
   // And for ACC_BITS = 43, P_SHIFT = 17
   parameter ACC_BITS = 43;

   // Positive scaling of proportional (integrator left alone)
   parameter P_SHIFT = 15;

   // To alter integrator scaling without changing proportional, keep
   // ACC_BITS-P_SHIFT constant.

   reg signed [15:0] 		    err_r1, err_r2, err_r3;
   reg signed [ACC_BITS-1:0] 	    u_full, u_full_r, u_full_r2;
   wire [ACC_BITS-17:0] 	    zero_padding = 0;
   reg signed [15:0] 		    ramp_centre_r;

   initial begin
      err_r1 <= 16'd0;
      err_r2 <= 16'd0;
      err_r3 <= 16'd0;
      u_full <= 0;
      u_full_r <= 0;
      u_full_r2 <= 0;
      ramp_centre_r <= 16'd0;
   end

   // DAC output
   assign pid_o = u_full[ACC_BITS-1:ACC_BITS-16] + out_offset_i;

   // Debugging
//   assign dac_pdata_o = {~err_r1[15],err_r1[14:2]};

   // Unsigned-to-signed conversions
   wire signed [17:0] p = {2'b0,p_i};
   wire signed [17:0] i = {2'b0,i_i};
//   wire signed [17:0] d = {2'b0,d_i};

   
//   wire signed [34:0] p_er = p * (err_r1-err_r2);
//   wire signed [33:0] i_er1 = i * err_r1;

   reg signed [34:0]  p_er;
   reg signed [33:0]  i_er1;

   always @(posedge clk) begin
      p_er <= p * (err_r1 - err_r2);
      i_er1 <= i * err_r1;
   end
				
   always @(posedge clk) begin
      if (!rst_n_i) begin
          // error reg stuff
          err_r1 <= 16'd0;
          err_r2 <= 16'd0;
          err_r3 <= 16'd0;	 
          // U stuff
          u_full <= {ramp_centre_i,zero_padding};
      end else begin
          // err reg stuff
          if(!sw_polar_i) err_r1 <= in_offset_i + $signed(err_i);
          else err_r1 <= -in_offset_i - $signed(err_i);
          // U stuff
          err_r2 <= err_r1;
          err_r3 <= err_r2;
	 
          // NEED TO REDUCE LENGTH OF U_FULL FOR EFFICIENT ADDITION
          u_full <= u_full + (p_er <<< P_SHIFT) + i_er1; 
	 
	 //	 u_full <= u_full + (((p_shift + i_i + d_i)*err_r1 -
	 // (p_shift + (d_i <<< 1))*err_r2 + d_i*err_r3) );
      end // else: !if(rst_n_i)
   end // always @ (posedge clk)
endmodule

`endif
