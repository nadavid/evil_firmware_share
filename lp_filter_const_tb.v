//-----------------------------------------------------------------------------
// Title         : lp_filter_tb
// Project       : EVIL
//-----------------------------------------------------------------------------
// File          : lp_filter_tb.v
// Author        : Alexander Hungenberg  <ahungenberg@phys.ethz.ch>
// Created       : 08.04.2013
// Last modified : 08.04.2013
//-----------------------------------------------------------------------------
// Description :
// Testbench for lp_filter
//-----------------------------------------------------------------------------
// Copyright (c) 2013 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 08.04.2013 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ps

`ifndef _LP_FILTER_CONST_TB_
 `define _LP_FILTER_CONST_TB_

 `ifndef _LP_FILTER_CONST_
  `include "lp_filter_const.v"
 `endif

module lp_filter_const_tb;

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg			clk;			// To UUT of lp_filter_const.v
   reg signed [15:0]	data_i;			// To UUT of lp_filter_const.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire signed [15:0]	data_o;			// From UUT of lp_filter_const.v
   // End of automatics


   lp_filter_const UUT(/*AUTOINST*/
		       // Outputs
		       .data_o		(data_o[15:0]),
		       // Inputs
		       .clk		(clk),
		       .data_i		(data_i[15:0]));

   always #5 clk = !clk;
   
   initial begin
      $dumpfile("icarus_compile/000_lp_filter_const_tb.vcd");
      $dumpvars(0,lp_filter_const_tb);

      clk = 1;
      data_i = 0;

      #98 data_i = 30000;
      #1000000 data_i = -30000;
      #1000000 $finish;
   end // initial begin

/* -----\/----- EXCLUDED -----\/-----
   reg [15:0] data_o_r2;
   always @(posedge clk) begin
      data_o_r2 <= data_o;
   end
   wire [15:0] data_o_deriv = data_o-data_o_r2;
 -----/\----- EXCLUDED -----/\----- */

endmodule // lp_filter_tb

`endif
