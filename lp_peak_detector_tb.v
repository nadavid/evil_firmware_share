//-----------------------------------------------------------------------------
// Title         : lp_peak_detector_tb
// Project       : EVIL
//-----------------------------------------------------------------------------
// File          : lp_peak_detector_tb.v
// Author        : Alexander Hungenberg  <ahungenberg@phys.ethz.ch>
// Created       : 15.04.2013
// Last modified : 15.04.2013
//-----------------------------------------------------------------------------
// Description :
// Testbench for lp_peak_detector
//-----------------------------------------------------------------------------
// Copyright (c) 2013 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 15.04.2013 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ps

`ifndef _LP_PEAK_DETECTOR_TB_
 `define _LP_PEAK_DETECTOR_TB_

 `ifndef _LP_PEAK_DETECTOR_
  `include "lp_peak_detector.v"
 `endif

module lp_peak_detector_tb;

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                  clk;                    // To UUT of lp_peak_detector.v
   reg signed [15:0]    data_i;                 // To UUT of lp_peak_detector.v
   reg                  peak_detected_rst_i;    // To UUT of lp_peak_detector.v
   reg [15:0]           peak_detected_ttl_i;    // To UUT of lp_peak_detector.v
   reg [15:0]           peak_threshold_i;       // To UUT of lp_peak_detector.v
   reg signed [15:0]    smoothing_i;            // To UUT of lp_peak_detector.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [15:0]          data_i_filtered_o;      // From UUT of lp_peak_detector.v
   wire [15:0]          data_slow_lp_o;         // From UUT of lp_peak_detector.v
   wire                 peak_detected_o;        // From UUT of lp_peak_detector.v
   // End of automatics

   lp_peak_detector UUT(/*AUTOINST*/
                        // Outputs
                        .peak_detected_o(peak_detected_o),
                        .data_slow_lp_o (data_slow_lp_o[15:0]),
                        .data_i_filtered_o(data_i_filtered_o[15:0]),
                        // Inputs
                        .clk            (clk),
                        .data_i         (data_i[15:0]),
                        .smoothing_i    (smoothing_i[15:0]),
                        .peak_threshold_i(peak_threshold_i[15:0]),
                        .peak_detected_ttl_i(peak_detected_ttl_i[15:0]),
                        .peak_detected_rst_i(peak_detected_rst_i));

   always #5 clk = !clk;
   
   initial begin
      $dumpfile("icarus_compile/000_lp_peak_detector_tb.vcd");
      $dumpvars(0,lp_peak_detector_tb);

      clk = 0;
      data_i = 0;
      smoothing_i = 3000;
      peak_threshold_i = 500;
      peak_detected_ttl_i = 100;
      peak_detected_rst_i = 1;

      #100000 data_i = 1000;
      #100000 data_i = 0;
      #10000000 data_i = -1000;
      #100000 data_i = 1000;
      #500000 data_i = 0;
      #600000 peak_detected_rst_i=0;
      #10000000 $finish;
   end // initial begin

endmodule // lp_peak_detector_tb

`endif
