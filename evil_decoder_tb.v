//-----------------------------------------------------------------------------
// Title         : evil_decoder_tb
// Project       : evil_fw
//-----------------------------------------------------------------------------
// File          : evil_decoder_tb.v
// Author        : Vlad Negnevitsky  <vlad@vlad-VirtualBox>
// Created       : 21.11.2012
// Last modified : 21.11.2012
//-----------------------------------------------------------------------------
// Description :
// Testbench for EVIL decoder, the I/O interface between the EVIL and host.
//-----------------------------------------------------------------------------
// Copyright (c) 2012 by TIQI, ETH This model is the confidential and
// proprietary property of TIQI, ETH and the possession or use of this
// file requires a written license from TIQI, ETH.
//------------------------------------------------------------------------------
// Modification history :
// 21.11.2012 : created
//-----------------------------------------------------------------------------

`ifndef _EVIL_DECODER_TB_
`define _EVIL_DECODER_TB_
`timescale 1ns/1ps

`ifndef _EVIL_DECODER_
 `include "evil_decoder.v"
`endif

module evil_decoder_tb;
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg			clk;			// To UUT of evil_decoder.v
   reg [7:0]		rx_data_i;		// To UUT of evil_decoder.v
   reg			rx_ready_i;		// To UUT of evil_decoder.v
   reg [7:0]		str0_i;			// To UUT of evil_decoder.v
   reg [7:0]		str1_i;			// To UUT of evil_decoder.v
   reg [7:0]		str2_i;			// To UUT of evil_decoder.v
   reg [7:0]		str3_i;			// To UUT of evil_decoder.v
   reg [7:0]		str4_i;			// To UUT of evil_decoder.v
   reg [7:0]		str5_i;			// To UUT of evil_decoder.v
   reg [7:0]		str6_i;			// To UUT of evil_decoder.v
   reg [7:0]		str7_i;			// To UUT of evil_decoder.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [15:0]		reg0_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg10_o;		// From UUT of evil_decoder.v
   wire [15:0]		reg11_o;		// From UUT of evil_decoder.v
   wire [15:0]		reg12_o;		// From UUT of evil_decoder.v
   wire [15:0]		reg13_o;		// From UUT of evil_decoder.v
   wire [15:0]		reg14_o;		// From UUT of evil_decoder.v
   wire [15:0]		reg15_o;		// From UUT of evil_decoder.v
   wire [15:0]		reg1_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg2_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg3_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg4_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg5_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg6_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg7_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg8_o;			// From UUT of evil_decoder.v
   wire [15:0]		reg9_o;			// From UUT of evil_decoder.v
   wire [7:0]		str0_o;			// From UUT of evil_decoder.v
   wire [7:0]		str1_o;			// From UUT of evil_decoder.v
   wire [7:0]		str2_o;			// From UUT of evil_decoder.v
   wire [7:0]		str3_o;			// From UUT of evil_decoder.v
   wire [7:0]		str4_o;			// From UUT of evil_decoder.v
   wire [7:0]		str5_o;			// From UUT of evil_decoder.v
   wire [7:0]		str6_o;			// From UUT of evil_decoder.v
   wire [7:0]		str7_o;			// From UUT of evil_decoder.v
   wire [7:0]		tx_data_o;		// From UUT of evil_decoder.v
   wire			tx_ready_o;		// From UUT of evil_decoder.v
   // End of automatics

   reg [7:0] 		prev_tx_data;
   
   initial begin
      $dumpfile("icarus_compile/000_evil_decoder_tb.vcd");
      $dumpvars(0,evil_decoder_tb);

      clk = 0;
      rx_data_i = 0;
      rx_ready_i = 0;
      str0_i = 8'hab;
      str1_i = 8'hba;

      #23;

      // Write some registers
      #10 set_reg(0, 16'hdead);
      set_reg(1, 16'hbeef);
      set_reg(2, 16'hcafe);
      set_reg(3, 16'hbabe);

      // Read some registers
      #10 get_reg(0);
      #100 get_reg(3);

      // Make the delay longer for reading registers
      #100 set_reg(14,16'd2);
      
      // Read again
      #100 get_reg(1);

      // Set stream count
      #1000 set_reg(15,16'd3);
      #100 get_stream(0);
      #1000 get_stream(1);

      #3000 set_reg(15,16'd0);
      #100 get_stream(0);

      #50000 $finish;
   end

   always @(posedge clk)
     if (tx_ready_o) prev_tx_data <= tx_data_o;
   
   always #5 clk = !clk;

   // Set a register value
   task set_reg;
      input [4:0] reg_ind;
      input [15:0] reg_data;

      begin
	 rx_ready_i = 0;
	 #10 rx_data_i = {3'b001, reg_ind};
	 rx_ready_i = 1;
	 #10 rx_ready_i = 0;
	 #10 rx_data_i = reg_data[7:0];
	 rx_ready_i = 1;
	 #10 rx_ready_i = 0;
	 #10 rx_data_i = reg_data[15:8];
	 rx_ready_i = 1;
	 #10 rx_ready_i = 0;
      end
   endtask

   // Get a stream
   task get_stream;
      input [4:0] reg_ind;

      begin
	 rx_ready_i = 0;
	 #10 rx_data_i = {3'b010, reg_ind};
	 rx_ready_i = 1;
	 #10 rx_ready_i = 0;
      end
   endtask
   
   // Get a register value
   task get_reg;
      input [4:0] reg_ind;

      begin
	 rx_ready_i = 0;
	 #10 rx_data_i = {3'b000, reg_ind};
	 rx_ready_i = 1;
	 #10 rx_ready_i = 0;
      end
   endtask
   
   evil_decoder #(
		 // Parameters
		 .min_div		(2'd3))
UUT(/*AUTOINST*/
    // Outputs
    .tx_data_o				(tx_data_o[7:0]),
    .tx_ready_o				(tx_ready_o),
    .str0_o				(str0_o[7:0]),
    .str1_o				(str1_o[7:0]),
    .str2_o				(str2_o[7:0]),
    .str3_o				(str3_o[7:0]),
    .str4_o				(str4_o[7:0]),
    .str5_o				(str5_o[7:0]),
    .str6_o				(str6_o[7:0]),
    .str7_o				(str7_o[7:0]),
    .reg0_o				(reg0_o[15:0]),
    .reg1_o				(reg1_o[15:0]),
    .reg2_o				(reg2_o[15:0]),
    .reg3_o				(reg3_o[15:0]),
    .reg4_o				(reg4_o[15:0]),
    .reg5_o				(reg5_o[15:0]),
    .reg6_o				(reg6_o[15:0]),
    .reg7_o				(reg7_o[15:0]),
    .reg8_o				(reg8_o[15:0]),
    .reg9_o				(reg9_o[15:0]),
    .reg10_o				(reg10_o[15:0]),
    .reg11_o				(reg11_o[15:0]),
    .reg12_o				(reg12_o[15:0]),
    .reg13_o				(reg13_o[15:0]),
    .reg14_o				(reg14_o[15:0]),
    .reg15_o				(reg15_o[15:0]),
    // Inputs
    .clk				(clk),
    .rx_data_i				(rx_data_i[7:0]),
    .rx_ready_i				(rx_ready_i),
    .str0_i				(str0_i[7:0]),
    .str1_i				(str1_i[7:0]),
    .str2_i				(str2_i[7:0]),
    .str3_i				(str3_i[7:0]),
    .str4_i				(str4_i[7:0]),
    .str5_i				(str5_i[7:0]),
    .str6_i				(str6_i[7:0]),
    .str7_i				(str7_i[7:0]));

endmodule // evil_decoder_tb
   
`endif //  `ifndef _EVIL_DECODER_TB_


