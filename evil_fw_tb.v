//-----------------------------------------------------------------------------
// Title         : evil_fw_tb
// Project       : evil_fw
//-----------------------------------------------------------------------------
// File          : evil_fw_tb.v
// Author        : Vlad Negnevitsky  <vlad@vlad-VirtualBox>
// Created       : 20.11.2012
// Last modified : 20.11.2012
//-----------------------------------------------------------------------------
// Description :
// Testbench for evil_fw; the top level of the EVIL board firmware.
//-----------------------------------------------------------------------------
// Copyright (c) 2012 by TIQI, ETHZ This model is the confidential and
// proprietary property of TIQI, ETHZ and the possession or use of this
// file requires a written license from TIQI, ETHZ.
//------------------------------------------------------------------------------
// Modification history :
// 20.11.2012 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ps

`ifndef _EVIL_FW_TB_
`define _EVIL_FW_TB_

module evil_fw_tb;

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg signed [9:0]     adc_a_i;                // To UUT of evil_fw.v
   reg                  adc_a_overflow;         // To UUT of evil_fw.v
   reg signed [9:0]     adc_b_i;                // To UUT of evil_fw.v
   reg                  adc_b_overflow;         // To UUT of evil_fw.v
   reg                  clk_i;                  // To UUT of evil_fw.v
   reg                  rxd;                    // To UUT of evil_fw.v
   reg                  trig_in;                // To UUT of evil_fw.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 adc_clk_o;              // From UUT of evil_fw.v
   wire                 adc_pdwn_o;             // From UUT of evil_fw.v
   wire [3:0]           dbg;                    // To/From UUT of evil_fw.v
   wire                 fast_dac_clk_o;         // From UUT of evil_fw.v
   wire [13:0]          fast_dac_o;             // From UUT of evil_fw.v
   wire                 txd;                    // From UUT of evil_fw.v
	
	wire slow_dac_serial_data_o;
		 wire       slow_dac_clk_o;
		 wire       slow_dac_sync_o;
	
   // End of automatics

   reg 			en_16_x_baud = 0;
   reg 			baud_count = 0;
   reg 			clk_100;
   wire [7:0] 		out_data;
   wire 		out_data_ready;
	
   
   evil_fw UUT(// Outputs
               .adc_clk_o               (adc_clk_o),
               .adc_pdwn_o              (adc_pdwn_o),
               .fast_dac_o              (fast_dac_o[13:0]),
               .fast_dac_clk_o          (fast_dac_clk_o),
					.slow_dac_serial_data_o(slow_dac_serial_data_o),
					.slow_dac_clk_o(slow_dac_clk_o),
					.slow_dac_sync_o(slow_dac_sync_o),
               .txd_o                   (txd),
               // Inouts
               .dbg                     (dbg[3:0]),
               // Inputs
               .clk_i                   (clk_i),
               .trig_in                 (trig_in),
               .adc_a_i                 (adc_a_i[9:0]),
               .adc_b_i                 (adc_b_i[9:0]),
               .adc_a_overflow          (adc_a_overflow),
               .adc_b_overflow          (adc_b_overflow),
               .rxd_i                   (rxd));

   initial begin
      $dumpfile("icarus_compile/000_evil_fw_tb.vcd");
      $dumpvars(0,evil_fw_tb);
      
      clk_i = 0;
      clk_100 = 0;
      adc_a_i = 0;
      adc_a_overflow = 0;
      adc_b_i = 0;
      adc_b_overflow = 0;
      rxd = 1;
      trig_in = 0;#10000;

      set_reg(12, 16'hcafe); // Set Reg 12 to 'cafe'
      set_reg(13, 16'hbabe); // Set Reg 13 to 'babe'
      set_reg(15, 16'd4); // 3 streamed bytes requested
      get_stream(0); // Get data from Port 0 (ADC)

      // Switch on ramp generator
      #26000 set_reg(0,16'h0006); // Ramp gen on, output is ramp gen
      set_reg(1,-10); // Ramp centre
      set_reg(2, 10000); // Ramp range
      set_reg(3, 40000); // Ramp frequency
      set_reg(15,30); // stream quantity
      set_reg(14,0); // stream delay (n = 400 + 512*this)
      //get_stream(2); // Get ramp MSB data
      #200000
      //#get_reg(16); // Get trigger point (stream length minus where the ramp was triggered in the sequence)
      
      // Disable Ramp and enable PID
      set_reg(6,16'd100); // P-Gain
      set_reg(7,16'd60000); // I-Gain
      set_reg(9,16'd30000);
      set_reg(10,16'd15);
      #20000 set_reg(0,16'b0000000000100001); // P Controller on, Peak detect on

      // Put in some ADC values
      #50000 adc_a_i = 2;
      #50000 adc_a_i = 0;
      #50000 adc_a_i = 400;
      #1000 adc_a_i = 0;
      
      #1000000 $finish;
   end

   // Set a register value
   task set_reg;
      input [4:0] reg_ind;
      input [15:0] reg_data;

      begin
         transmit({3'b001, reg_ind});
         transmit(reg_data[7:0]);
         transmit(reg_data[15:8]);
      end
   endtask

   // Get a register value
   task get_reg;
      input [4:0] reg_ind;
      
      begin
         transmit({3'b000, reg_ind});
      end
   endtask

   task get_stream;
      input [4:0] reg_ind;
      
      begin
         transmit({3'b010, reg_ind});
      end
   endtask
      
   // Transmit byte over UART
   task transmit;
      input [7:0] in;

      integer     t;
      begin
         rxd = 0; #320;
         for(t = 0; t<8; t = t + 1) begin
            rxd = in[t]; #320;
         end
         rxd = 1; #320;
         #320;
      end
   endtask // transmit
   
   // 32 MHz (31.25 ns) clock
   always #15.625 clk_i = !clk_i;

   // 100 (~96) MHz clock
   always #5 clk_100 = !clk_100;

   // UART, purely for receiving output of UUT.
   // *Not* implemented in the hardware!

   // Baudrate generator
   always @(posedge clk_100) begin
      if (baud_count == 4'd1) begin
	 baud_count <= 4'd0;
	 en_16_x_baud <= 1'd1;
      end else begin
	 baud_count <= baud_count + 4'd1;
	 en_16_x_baud <= 1'd0;
      end
   end

   // Uart
   uart_rx Inst_uart_rx(
			.serial_in(txd),
			.read_buffer(1'd1),
			.reset_buffer(1'd0),
			.en_16_x_baud(en_16_x_baud),
			.clk(clk_100),
			.data_out(out_data),
			.buffer_data_present(out_data_ready),
			.buffer_half_full(),
			.buffer_full()
			); 
   
endmodule // evil_fw_tb

`endif
