module UARTExample_tb;

   reg rx;
   wire tx;
   reg 	extclk;

   UARTExample UUT(
		   .extclk(extclk),
		   .rx(rx),
		   .tx(tx)
		   );

   integer k;

   reg [7:0] data = "a";
   
   initial begin
      extclk = 0;
      rx = 1;
      #200 transmit(8'h6c);
      #1000 transmit(8'h09);
      transmit("0");
      transmit("1");
      transmit("2");
      transmit("3");
      transmit("4");
      transmit("5");
      transmit("6");
      transmit("7");
   end

   task transmit;
      input [7:0] in;

      integer 	  t;
      begin
	 rx = 0; #160;
	 for(t = 0; t<8; t = t + 1) begin
	    rx = in[t]; #160;
	 end
	 rx = 1; #160;
	 #5000;
      end
   endtask // transmit
   
   // 33 1/3 MHz clock
   always begin
      #7.5 extclk = !extclk;
   end

endmodule // UARTExample_tb
