#!/usr/bin/python3

# Monitor the serial port
import serial
import numpy as np
import matplotlib.pyplot as pl

port = 4
len = 2000
samples = 100
traces = 100

if __name__ == "__main__":
    port = 4
    ser = serial.Serial(
        port,
        3000000,
        timeout=1);

    adc_data = np.array(bytearray(ser.read(1)),dtype='int8')
    x=np.arange(adc_data.size)    
    pl.ion() # interactive mode
    fig = pl.figure()
    ax = fig.add_subplot(111)
    #import pdb;pdb.set_trace()
    line, = ax.plot(adc_data)
    pl.show()
    
    for cnt in range(traces):
        #ax.cla()
        adc_data = np.array(bytearray(ser.read(len)),dtype='int8')
        x=np.arange(x[-1],x[-1]+adc_data.size)
        ax.plot(x,adc_data)
        fig.canvas.draw()
        if x[-1] > len*20:
            x=np.array([0])
            ax.cla()
        

