#!/usr/bin/python3

# Monitor the serial port
import serial, struct
import numpy as np
import matplotlib.pyplot as pl
from time import sleep

port = 4
len = 2000
samples = 100
traces = 100

def set_reg_u(reg, value, serport):
    bs = struct.pack('<BH', # byte, then 16b int, little-endian
                     ((0x1 & 0x7) << 5) | (reg & 0x1f),
                     value)
    serport.write(bs)

def set_reg_s(reg, value, serport):
    bs = struct.pack('<Bh', # byte, then 16b int, little-endian
                     ((0x1 & 0x7) << 5) | (reg & 0x1f),
                     value)
    serport.write(bs)
    
def get_reg_u(reg, serport):
    bs = struct.pack('<B',
                     ((0x0 & 0x7) << 5) | (reg & 0x1f)
                     )
    serport.write(bs)
    return struct.unpack('H',serport.read(2))[0]

def get_reg_s(reg, serport):
    bs = struct.pack('<B',
                     ((0x0 & 0x7) << 5) | (reg & 0x1f)
                     )
    serport.write(bs)
    return struct.unpack('h',serport.read(2))[0]

def get_stream_s(reg, num, serport):
    bs=struct.pack('<B',
                   ((0x2 & 0x7) << 5) | (reg & 0x1f)
                     )
    serport.write(bs)
    return np.array(bytearray(serport.read(num)),dtype='int8')

def get_stream_u(reg, num, serport):
    bs=struct.pack('<B',
                   ((0x2 & 0x7) << 5) | (reg & 0x1f)
                     )
    serport.write(bs)
    return np.array(bytearray(serport.read(num)),dtype='uint8')

if __name__ == "__main__":
    port = 4
    ser = serial.Serial(
        port,
        3000000,
        timeout=10)

    # Test read/write
    for k in range(32):
        #set_reg_u(k,k,ser)
        pass
    for k in range(32):
        print("Expected: ",k,", seen: ",get_reg_u(k,ser))

    # Test streaming: stream data from the ADCs
    # speed; 187.05ksps / (value + 511/512)
    # for example, to get 1ksps, k = 187.05-511/512 = 186
    # ~10 ksps, k = 18
    stream_pts = 1000 # 65535 = max
    speed_ksps = 180 # 180 = max
    set_reg_u(14, round(187.05/speed_ksps-511/512), ser) 
    set_reg_u(15,stream_pts,ser) # number of points to transmit
    pl.plot(get_stream_s(0,stream_pts,ser))
    pl.plot(get_stream_s(1,stream_pts,ser))

    # Ramp generator
    set_reg_s(1, -1, ser); # Ramp centre
    set_reg_u(2, 10000, ser); # Ramp range
    set_reg_u(3, 1000, ser); # Ramp frequency
    set_reg_u(0, 0x0006, ser); # Turn ramp gen on, select output = rampgen    
    pl.plot(get_stream_s(2,stream_pts,ser)); # Get ramp generator MSBs

    #for k in range(10):
    #    for r in range(0,30):
    #        set_reg_u(2,10000+1000*r,ser)
    #        sleep(0.1)

    sleep(2)

    # # PID
    set_reg_s(4, 120, ser); # Input offset
    set_reg_s(5, 0, ser); # Output offset    
    set_reg_u(6, 25000, ser); # P gain
    set_reg_u(7, 0, ser); # I gain
    set_reg_u(0, 0x0001, ser); # Turn PID on, ramp gen off, select output = PID

    # Sweep through output offset
#    for k in range(5):
#        for r in range(-3200,3200):
#            set_reg_s(5,10*r,ser)
#    set_reg_s(5,0,ser)

    # Sweep through input offset
#    for k in range(5):
#        for r in range(-5120,5110):
#            set_reg_s(4,r//10,ser)
#    set_reg_s(4,120,ser)
    
    pl.plot(get_stream_s(3, stream_pts, ser))
    
    pl.show()
    
