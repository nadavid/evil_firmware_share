//-----------------------------------------------------------------------------
// Title         : evil_decoder
// Project       : evil_fw
//-----------------------------------------------------------------------------
// File          : evil_decoder.v
// Author        : Vlad Negnevitsky  <vlad@vlad-VirtualBox>
// Created       : 21.11.2012
// Last modified : 21.11.2012
//-----------------------------------------------------------------------------
// Description :
// I/O decoder for EVIL. Interprets commands from PC into various actions.
//-----------------------------------------------------------------------------
// Copyright (c) 2012 by TIQI, ETHZ This model is the confidential and
// proprietary property of TIQI, ETHZ and the possession or use of this
// file requires a written license from TIQI, ETHZ.
//------------------------------------------------------------------------------
// Modification history :
// 21.11.2012 : created
//-----------------------------------------------------------------------------

`ifndef _EVIL_DECODER_
 `define _EVIL_DECODER_
 `timescale 1ns/1ps

module evil_decoder(
		    input 	     clk,

		    // Communications
		    input [7:0]      rx_data_i,
		    input 	     rx_ready_i,
		    output reg [7:0] tx_data_o,
		    output 	     tx_ready_o,

		    // Triggering from ramp generator
		    input ramp_trig_i,

		    // Input data streams
		    input [7:0]      str0_i,
		    input [7:0]      str1_i,
		    input [7:0]      str2_i,
		    input [7:0]      str3_i,
		    input [7:0]      str4_i,
		    input [7:0]      str5_i,
		    input [7:0]      str6_i,
		    input [7:0]      str7_i,

		    // Output data streams
		    output [7:0]     str0_o,
		    output [7:0]     str1_o,
		    output [7:0]     str2_o,
		    output [7:0]     str3_o,
		    output [7:0]     str4_o,
		    output [7:0]     str5_o,
		    output [7:0]     str6_o,
		    output [7:0]     str7_o, 

		    // Output data regs
		    output [15:0]    reg0_o,
		    output [15:0]    reg1_o,
		    output [15:0]    reg2_o,
		    output [15:0]    reg3_o,
		    output [15:0]    reg4_o,
		    output [15:0]    reg5_o,
		    output [15:0]    reg6_o,
		    output [15:0]    reg7_o,
		    output [15:0]    reg8_o,
		    output [15:0]    reg9_o,
		    output [15:0]    reg10_o,
		    output [15:0]    reg11_o,
		    output [15:0]    reg12_o,
		    output [15:0]    reg13_o,
		    output [15:0]    reg14_o,
		    output [15:0]    reg15_o
		    );

   // Version (to be stored in Register 31 by default)
   // Last two digits are decimal point
   parameter VERSION = 100;

   // Opcodes
   parameter READ_FROM_REG     = 8'b000xxxxx;
   parameter WRITE_TO_REG    = 8'b001xxxxx;
   parameter READ_FROM_STREAM  = 8'b010xxxxx;
   parameter WRITE_TO_STREAM = 8'b011xxxxx;

   // Register array (currently top 16 unused)
   reg [15:0] 			     regbank [31:0];
   integer 			     bankc;
   initial begin
      for(bankc = 0; bankc < 32; bankc = bankc + 1) begin
	 regbank[bankc] = 0;
      end
      regbank[0] = 16'h0006; // control reg: start ramp mode; PID off initially
      regbank[9] = 16'd1;  // peak_smoothing
      regbank[10] = 16'd1; // peak_threshold
      regbank[15] = 16'd2500; // length of streaming: default 1000
      regbank[31] = VERSION; // version configured in top-level module (evil_fw.v)
   end
   
   // Opcode and two data words (if needed)
   reg [7:0] 			  opcode_r = 8'd0, rx_data_lsb = 8'd0, rx_data_msb = 8'd0;
   wire [4:0] 			  reg_idx = opcode_r[4:0]; // register index

   reg [15:0] 			  selected_reg = 16'd0;
   reg [15:0] 			  trig_index = 16'd0;
   always @(posedge clk) begin
      if (reg_idx == 5'd16) selected_reg <= trig_index;
      else selected_reg <= regbank[reg_idx];
   end
   
   // State machine
   reg [5:0] 			  state = 6'd0;

   // State machine states
   parameter IDLE = 6'd0;
   parameter RX_BYTE_LSB = 6'd1; // receive LSB byte
   parameter RX_BYTE_MSB = 6'd2; // receive MSB byte
   parameter WRITE_REG = 6'd3; // write full 16 bits to register
   parameter TX_BYTE_LSB = 6'd4; // transmit LSB byte
   parameter TX_BYTE_MSB = 6'd5; // transmit MSB byte
   parameter RX_STREAM = 6'd6; // receive stream until stream_counter = 0
   parameter TX_STREAM = 6'd7; // transmit stream until stream_counter = 0
   
   // TX counter (constantly ticks, sequencing the transmitted data)
   reg [24:0] 			  tx_counter = 25'd0;

   // tx_counter_en logic
   reg 				  tx_counter_en = 1'd0;
   always @(posedge clk) begin
      
      tx_counter_en <=   !tx_ready_o &&
			 ( (state == TX_BYTE_LSB) 
			 | (state == TX_BYTE_MSB) 
			 | (state == TX_STREAM));
   end
   
   always @(posedge clk) begin
      if (tx_counter_en) begin
	 tx_counter <= tx_counter + 25'd1;
      end else begin
	 tx_counter <= 25'd0;
      end
   end

   // Byte stream counter (register 15)
   parameter stream_count_reg = 15;
   reg [15:0] stream_counter = 16'd0;
   wire       stream_done = ( stream_counter == 16'd0 );
   always @(posedge clk) begin
      if (state == IDLE) stream_counter <= regbank[stream_count_reg];
      else if ( (state == TX_STREAM) && tx_ready_o ) begin
	 stream_counter <= stream_counter - 16'd1;
      end
   end
   
   // Byte stream rate timer (register 14)
   parameter tx_interval = 14;

   // min_div used so that maximum rate is still slow enough that TX FIFO
   // isn't overwhelmed
   parameter min_div = 9'd400;
   assign tx_ready_o = ({regbank[tx_interval], min_div} == tx_counter);   

   initial begin
      opcode_r = 0;
      rx_data_lsb = 0;
      rx_data_msb = 0;
      state = IDLE;
      tx_counter = 25'd0;
      stream_counter = 16'd0;
   end
   
   // Next-state logic
   always @(posedge clk) begin
      case (state)
	IDLE: begin
	   if (rx_ready_i) begin
	      opcode_r <= rx_data_i; // save data from UART
	      casex (rx_data_i) 
		// decode opcodes
		READ_FROM_REG: state <= TX_BYTE_LSB;
		WRITE_TO_REG: state <= RX_BYTE_LSB;
		READ_FROM_STREAM: begin
		   state <= TX_STREAM;
		   trig_index <= 16'd0;
		end
		WRITE_TO_STREAM: state <= RX_STREAM;
		default: state <= IDLE;
	      endcase // case rx_data_i
	   end else state <= IDLE;
	end // case: state:...
	RX_BYTE_LSB: begin
	   if (rx_ready_i) begin
	      rx_data_lsb <= rx_data_i; // save data from UART
	      state <= RX_BYTE_MSB;
	   end else state <= RX_BYTE_LSB;
	end
	RX_BYTE_MSB: begin
	   if (rx_ready_i) begin
	      rx_data_msb <= rx_data_i;
	      state <= WRITE_REG;
	   end else state <= RX_BYTE_MSB;
	end
	WRITE_REG: begin
	   // write into target register all at once
	   regbank[reg_idx] <= {rx_data_msb, rx_data_lsb};
	   state <= IDLE;
	end
	TX_BYTE_LSB: begin
	   tx_data_o <= selected_reg[7:0];
	   if (tx_ready_o) state <= TX_BYTE_MSB;
	   else state <= TX_BYTE_LSB;
	end
	TX_BYTE_MSB: begin
	   tx_data_o <= selected_reg[15:8];
	   if (tx_ready_o) state <= IDLE;
	   else state <= TX_BYTE_MSB;
	end
	RX_STREAM: begin
	   if (stream_done) state <= IDLE;
	   else state <= RX_STREAM;
	end
	TX_STREAM: begin
	   case(reg_idx)
	     5'd0: tx_data_o <= str0_i;
	     5'd1: tx_data_o <= str1_i;
	     5'd2: tx_data_o <= str2_i;
	     5'd3: tx_data_o <= str3_i;
        5'd4: tx_data_o <= str4_i;
        5'd5: tx_data_o <= str5_i;
        5'd6: tx_data_o <= str6_i;
        5'd7: tx_data_o <= str7_i;
	     // etc
	     default: tx_data_o <= 0;
	   endcase // case (reg_idx)
	   if (ramp_trig_i && (trig_index == 16'd0)) trig_index <= stream_counter;
	   if (stream_done) state <= IDLE;
	   else state <= TX_STREAM;
	end
	default: state <= IDLE;
      endcase // case state
   end
   
   // Plumbing
   assign reg0_o = regbank[0];
   assign reg1_o = regbank[1];
   assign reg2_o = regbank[2];
   assign reg3_o = regbank[3];
   assign reg4_o = regbank[4];
   assign reg5_o = regbank[5];
   assign reg6_o = regbank[6];
   assign reg7_o = regbank[7];
   assign reg8_o = regbank[8];
   assign reg9_o = regbank[9];
   assign reg10_o = regbank[10];
   assign reg11_o = regbank[11];
   assign reg12_o = regbank[12];
   assign reg13_o = regbank[13];
   assign reg14_o = regbank[14];
   assign reg15_o = regbank[15];

endmodule // evil_decoder

`endif //  `ifndef _EVIL_DECODER_

