//-----------------------------------------------------------------------------
// Title         : lp_filter
// Project       : EVIL
//-----------------------------------------------------------------------------
// File          : lp_filter.v
// Author        : Alexander Hungenberg  <ahungenberg@phys.ethz.ch>
// Created       : 08.04.2013
// Last modified : 08.04.2013
//-----------------------------------------------------------------------------
// Description :

// Simple filter design

//-----------------------------------------------------------------------------
// Copyright (c) 2013 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 08.04.2013 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ps

`ifndef _LP_FILTER_CONST_
 `define _LP_FILTER_CONST_

module lp_filter_const(
		 input 			  clk,

//		 input 			  use_gated_clock_i,
		 input signed [15:0] 	  data_i,
		 output reg signed [15:0] data_o
		 );

   parameter acc_bits = 35;
   parameter gated_clock_bits = 8;
   parameter use_gated_clock = 1;

   reg signed [15:0] 				  data_last_i = 0;
   reg signed [acc_bits:0] 			  data_r = 0;

   wire signed [acc_bits:0] 			  data_temp = data_last_i - data_o_w;

   wire signed [15:0] 				  data_o_w;
   
   assign data_o_w = data_r[acc_bits:acc_bits-15];

   initial begin
      data_o <= 0;
   end

   reg [gated_clock_bits-1:0] 					  gated_clock_counter_r = 0;
   always @(posedge clk) begin
      if (use_gated_clock)
	 gated_clock_counter_r <= gated_clock_counter_r + 1;
      else
	 gated_clock_counter_r <= 0;
   end

   // the filter works with an implicit bitshift. Our accumulation register has a length of
   // 36 bits and only the top 16 bits are used. In this case the time constant multiplied is 1
   // but due to the cutting of the excess 20 bits, this results in a real time constant of
   // 1*2^-20.
   
   always @(posedge clk) begin
      data_last_i <= data_i;
      if (gated_clock_counter_r == 0) begin
	 data_r <= data_r + data_temp;      
	 data_o <= data_o_w;
      end
   end

endmodule // lp_filter

`endif
