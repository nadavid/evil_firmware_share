//                              -*- Mode: Verilog -*-
// Filename        : ramp_gen.v
// Description     : Ramp generator for PID
// Author          : Vlad Negnevitsky
// Created On      : Fri Oct 05 00:08:44 2012
// Last Modified By: 
// Last Modified On: Fri Oct 05 00:08:44 2012
// Update Count    : 0
// Status          : Unknown, Use with caution!


`ifndef _RAMP_GEN_
 `define _RAMP_GEN_

   // Top 16 bits of long_cnt always go from 0 up to range_i-1, then
   // return to 0. They are added to (centre_i-range_i/2) to calculate
   // the actual ramp values.

   // long_cnt is always incremented by cnt_add, which is range_i *
   // speed_i. This ensures the ramp rate is proportional to the
   // vertical range, such that the ramp sawtooth retains a constant
   // period.

   // The width of long_cnt determines the minimum and maximum counter
   // rate.

   // Example: range_i = 2 (minimum range), speed_i = 1 (minimum
   // speed) and long_cnt is 46 bits long. Every cycle, long_cnt will
   // be incremented by range_i * speed_i = 2; the sawtooth resets
   // once the top 16 bits exceed 1. The bit difference is given by
   // 46-16, or 30 bits. So it will take 2**30 clock cycles for the
   // step to propagate to the relevant bits. For a 30 MHz clock, it
   // will take 35.8 seconds to do one sweep.

   // Of course the sweep rate will remain constant when range_i is
   // increased, since as the counter accumulation rate grows, so does
   // the counter's upper limit; the sweep rate can be smoothly
   // altered by increasing speed_i. At a maximum of 2**16-1, the
   // sawtooth will have a sweep period of 540 us. This counter could
   // easily be extended for faster periods simply by bit shifting the
   // range_i * speed_i value with a variable shift, with perhaps two
   // overlapping modes.

module ramp_gen(
		input 		  clk_i,
		input 		  rst_n_i,
		input [15:0] 	  centre_i,
		input [15:0] 	  range_i,
		input [15:0] 	  speed_i,
		output reg [15:0] ramp_o,
		output reg ramp_trig_o
		);

   parameter cnt_wid = 44;
   parameter slope = 3; // Down-ramp is 2^3 = 8x faster than up-ramp
   
   reg [cnt_wid-1:0] 		  long_cnt = 0;
   reg 				  ramp_up = 1'd0, ramp_up_r = 1'd0; // Keep track of whether ramping up or down
   
   wire [15:0] 			  ramp_offset = centre_i - (range_i >> 1);
   wire [31:0] 			  cnt_inc = range_i * speed_i;
   wire [15:0] 			  long_cnt_top = long_cnt[cnt_wid-1:cnt_wid-16];

   // Hi when long_cnt is above its range (or underflows when ramping down)
   // Note: underflow doesn't matter too much, as long as during it,  | long_cnt_top | < ramp_offset .
   wire 			  long_cnt_clr = (long_cnt_top >= range_i);
   reg 				  long_cnt_clr_old = 1'd0;
   
   // Initial block
   initial begin
      ramp_o = 0;
      ramp_trig_o = 1;
   end

   // Long-count increment
   always @(posedge clk_i) begin
      if (!rst_n_i) begin
	 ramp_o <= 0;
	 long_cnt <= 0;
	 ramp_trig_o <= 1;
      end else begin
	 ramp_up_r <= ramp_up;
	 ramp_trig_o <= {ramp_up,ramp_up_r} == 2'b10; // end of ramp; polarity reversal
	 
	 long_cnt_clr_old <= long_cnt_clr;
	 
	 ramp_o <= long_cnt_top + ramp_offset;

	 if(long_cnt_clr & !long_cnt_clr_old) begin
	    ramp_up <= ~ramp_up; // change direction
	 end
	 
	 if(ramp_up) begin
	    long_cnt <= long_cnt + cnt_inc;
	 end else begin
	    long_cnt <= long_cnt - (cnt_inc << slope);
	 end
      end // else: !if(!rst_n_i)
   end
endmodule // ramp_gen

`endif