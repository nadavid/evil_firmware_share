//-----------------------------------------------------------------------------
// Title         : lp_filter
// Project       : EVIL
//-----------------------------------------------------------------------------
// File          : lp_filter.v
// Author        : Alexander Hungenberg  <ahungenberg@phys.ethz.ch>
// Created       : 08.04.2013
// Last modified : 08.04.2013
//-----------------------------------------------------------------------------
// Description :

// Simple filter design

//-----------------------------------------------------------------------------
// Copyright (c) 2013 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 08.04.2013 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ns

`ifndef _LP_FILTER_
 `define _LP_FILTER_

module lp_filter(
		 input 		      clk,

		 input signed [15:0]  data_i,
		 input signed [15:0] 	      smoothing_i, // real smoothing is smoothing * 2^-16
		 output reg signed [15:0] data_o
		 );

   parameter acc_bits = 31;
   parameter gated_clock_bits = 10;
   parameter use_gated_clock = 1;

   reg signed [15:0] 				  data_last_i = 0;
   reg signed [acc_bits:0] 			  data_r = 0;
   reg signed [acc_bits:0] smoothing_multiplied_r = 0;

   
   wire signed [15:0] 				  data_o_w;
   assign data_o_w = data_r[acc_bits:acc_bits-15];
   reg signed [15:0] 				  data_temp = 0;

   initial begin
      data_o <= 0;
   end
   
   reg [gated_clock_bits-1:0]                     gated_clock_counter_r = 0;
   always @(posedge clk) begin
      if (use_gated_clock)
	gated_clock_counter_r <= gated_clock_counter_r + 1;
      else
	gated_clock_counter_r <= 0;
   end
   
   always @(posedge clk) begin
      data_last_i <= data_i;
      data_temp <= data_last_i - data_o_w;
      smoothing_multiplied_r <= smoothing_i*data_temp;
      if (gated_clock_counter_r == 0) begin
	 data_r <= data_r + smoothing_multiplied_r;
	 data_o <= data_o_w;
      end
   end

endmodule // lp_filter

`endif
