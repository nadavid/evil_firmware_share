library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DAC_controller is
  Port ( 
    CLK      : in  STD_LOGIC;
    RST      : in  STD_LOGIC;
    DAC_DATA : in  STD_LOGIC_VECTOR (15 downto 0);
    start    : in  STD_LOGIC;
    rdy      : out STD_LOGIC;
    DAC_SYNC : out STD_LOGIC;
    DAC_SCK  : out STD_LOGIC;
    DAC_MOSI : out STD_LOGIC
    );
end DAC_controller;

architecture Behavioral of DAC_controller is

  type state_type is (idle,ready,send,dummy,check,syncoff);
  signal state    : state_type;
  signal StatexDN, StatexDP : state_type;
  signal DAC_SEND : std_logic_vector(15 downto 0);

begin

----------------------------------------------------------------------
  reverse : process(DAC_DATA)
  begin
    for i in 15 downto 0 loop
      DAC_SEND(i) <= DAC_DATA(15 - i); 
    end loop;
  end process reverse;
----------------------------------------------------------------------
  
----------------------------------------------------------------------  
  thestatemachine : process(CLK, RST)
    variable index : integer range 0 to 16 := 0;
  begin
    if (RST = '1') then
      rdy      <= '0'; 
      index    := 0;
      DAC_MOSI <= '0'; 
      DAC_SCK  <= '0';
      DAC_SYNC <= '0';
      state    <= idle;
          
	 elsif (rising_edge(CLK)) THEN
      case state is
        when idle =>
          DAC_SCK  <= '0';
          DAC_SYNC <= '1';
          index    :=  0 ;
          DAC_MOSI <= '0';
          if (start = '1') then
            state    <= ready;
            rdy      <= '1'; 
          else
            state    <= idle;
            rdy      <= '0'; 
          end if;
        when ready =>
          rdy      <= '0'; 
          DAC_SCK  <= '0';
          DAC_SYNC <= '0';
          state    <= send;
        when send =>
          rdy      <= '0'; 
          DAC_MOSI <= DAC_SEND(index);
          DAC_SYNC <= '0';
          DAC_SCK  <= '1';
			 index    := index + 1;
          if (index = 16) then
            state <= syncoff;
          else
            state <= ready;
          end if;
        when syncoff =>
		    DAC_SCK  <= '0';
		    state <= idle;
		  when others =>
			   state <= idle;                                 
      end case; 
    end if;     
  end process thestatemachine;
----------------------------------------------------------------------

end Behavioral;

