//-----------------------------------------------------------------------------
// Title         : lp_peak_detector
// Project       : EVIL
//-----------------------------------------------------------------------------
// File          : lp_peak_detector.v
// Author        : Alexander Hungenberg  <ahungenberg@phys.ethz.ch>
// Created       : 15.04.2013
// Last modified : 15.04.2013
//-----------------------------------------------------------------------------
// Description :
// This module will notify via the peak_detected output if there was
// a quick and significant deviation of the input.
//-----------------------------------------------------------------------------
// Copyright (c) 2013 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 15.04.2013 : created
//-----------------------------------------------------------------------------

`timescale 1ns/1ps

`ifndef _LP_PEAK_DETECTOR_
 `define _LP_PEAK_DETECTOR_

 `ifndef _LP_FILTER_
  `include "lp_filter.v"
 `endif

module lp_peak_detector(input 		    clk, 
			               
			               input signed [15:0] data_i,
			               input signed [15:0] smoothing_i,
			               input [15:0]        peak_threshold_i,
                        input [15:0]        peak_detected_ttl_i, 
			               input               peak_detected_rst_i,

			               output reg          peak_detected_o,
			               output reg [15:0]   data_slow_lp_o,
                        output reg [15:0]   data_i_filtered_o);
   
   initial begin
      peak_detected_o <= 0;
      data_slow_lp_o <= 0;
   end

   // Make the input an register for optimization purposes
   reg signed [15:0] 			    data_i_r = 0;
   reg signed [15:0]              smoothing_i_r = 0;
   reg [15:0]                     peak_threshold_i_r = 0;
   reg [15:0]                     peak_detected_ttl_i_r = 0;
   always @(posedge clk) begin
      data_i_r <= data_i;
      smoothing_i_r <= smoothing_i;
      peak_threshold_i_r <= peak_threshold_i;
      peak_detected_ttl_i_r <= peak_detected_ttl_i;
   end

   wire [15:0] data_i_slow_lp;
   wire [15:0] data_i_lp_diff;

   assign data_i_lp_diff = data_i_r - data_i_slow_lp;
   
   lp_filter lp_filter_slow_i(.clk(clk),
			                     .data_i(data_i_r),
			                     .smoothing_i(16'd1),
			                     .data_o(data_i_slow_lp));

   wire [15:0] data_i_filtered;
   reg [15:0]  data_i_filtered_abs = 16'd0;
   
   always @(posedge clk) begin
      if (data_i_filtered[15]) data_i_filtered_abs <= -data_i_filtered;
      else data_i_filtered_abs <= data_i_filtered;
      
      data_i_filtered_o <= data_i_filtered_abs;
   end

   lp_filter lp_filter_i(.clk(clk),
			                .data_i(data_i_lp_diff),
			                .smoothing_i(smoothing_i_r),
			                .data_o(data_i_filtered));

   // create a gated clock to be used with the ttl for peak_detection signal
   // we have a 16bit configuration register for ttl and want to make it configurable
   // up to a length of about one second (a 96MHz counter would use 27 bit)
   // so we introduce a 11 bit gated clock
   reg [10:0] peak_detection_gated_clock = 0;
   always @(posedge clk) begin
      peak_detection_gated_clock <= peak_detection_gated_clock + 1;
   end
   
   reg [15:0] peak_detection_rst_counter_r = 0;
   
   always @(posedge clk) begin
      if (data_i_filtered_abs > peak_threshold_i_r) begin
	      peak_detected_o <= 1;
      end else begin
         // we will start the ttl expand only after the peak has gone
         if (peak_detected_o && peak_detection_gated_clock == 0)
           peak_detection_rst_counter_r <= peak_detection_rst_counter_r + 1;
	      if (peak_detection_rst_counter_r > peak_detected_ttl_i_r) begin
	         peak_detected_o <= 0;
	         peak_detection_rst_counter_r <= 0;
	      end
      end

      data_slow_lp_o <= data_i_slow_lp;

      // a reset will clear the peak detection signal and ttl counter immediately
      if (peak_detected_rst_i == 0) begin
         peak_detected_o <= 0;
         peak_detection_rst_counter_r <= 0;
      end
   end
endmodule // lp_peak_detector

`endif
