library IEEE; 
use IEEE.STD_LOGIC_1164.ALL;  
use IEEE.NUMERIC_STD.ALL; 
use IEEE.STD_LOGIC_UNSIGNED.ALL; 

entity spi_dac is
  Port ( I_CLK_REF    : in  STD_LOGIC;
         SPI_DATA     : in std_logic_VECTOR(11 downto 0);
         DAC_SDATA    : out  STD_LOGIC;
         DAC_SYNC     : out  STD_LOGIC;
         DAC_CLK      : out STD_LOGIC
         ); 
end spi_dac;

architecture Behavioral of spi_dac is

  COMPONENT DAC_controller is
    Port ( 
      CLK      : in  STD_LOGIC;
      RST      : in  STD_LOGIC;
      DAC_DATA : in  STD_LOGIC_VECTOR (15 downto 0);
      start    : in  STD_LOGIC;
      rdy      : out STD_LOGIC;
      DAC_SYNC : out STD_LOGIC;
      DAC_SCK  : out STD_LOGIC;
      DAC_MOSI : out STD_LOGIC
      );
  end COMPONENT;

  type state_type is (idle, DataToSend, writetodac);
  signal state       : state_type;

  signal DATA          : std_logic_VECTOR(15 downto 0) := "0000000000000000";
  signal RST           : std_logic := '0';
  signal DacClk        : std_logic := '0';
  signal ready         : std_logic := '0';
  signal startREG      : std_logic := '0';
  signal ADC_REG       : std_logic_VECTOR(11 downto 0)  := "000000000000"; 
  signal ToSend        : std_logic_VECTOR(15 downto 0) := "0000000000000000"; 

begin

  DAC_CLK   <= DacClk;
  RST       <= '0';

  instDac : DAC_controller 
    Port Map( 
      CLK      => I_CLK_REF,
      RST      => RST,
      DAC_DATA => ToSend,
      start    => startREG,
      rdy      => ready,
      DAC_SYNC => DAC_SYNC,
      DAC_SCK  => DacClk,
      DAC_MOSI => DAC_SDATA
      );

  dataOne : process(I_CLK_REF)    
  begin 
    if(rising_edge(I_CLK_REF)) then
      
      case state is
        
        when idle =>
          startREG  <= '0';
          ADC_REG   <= (NOT SPI_DATA(11)) & (SPI_DATA(10 downto 0)); --first test board
          state     <= DataToSend;
          
        when DataToSend =>
          ToSend    <= "0001" & ADC_REG;
          state     <= writetodac;
          
        when writetodac =>
          startREG  <= '1';
          if (ready = '1') then
            state     <= idle;
          else		
            state     <= writetodac;
          end if;
          
        when others =>
          state     <= idle;	
          
      end case;		  
      
    end if;	  
  end process dataOne;

end Behavioral;
